function p = simple_gt(index_Nc, p0, p0_sum, num_bi, confidencelevel)
    if nargin == 4
        confidencelevel = 1.65;  % ? 1.65 or 1.96
    end
    
    
     % smooth
    len = length(index_Nc);
    r_mat = [log(index_Nc(1, :)); ones(1, len)]';
    Nr_mat = log(index_Nc(2, :))';
    coeff = regress(Nr_mat, r_mat);  % linear regression of Nr_mat and r_mat: r_mat*coeff = Nr_mat
    a = coeff(1, 1);
    b = coeff(2, 1);
    useY = 0;
    p = zeros(2, len);
    
    for i = 1:len
        r = index_Nc(1, i);
        Nr = index_Nc(2, i);
        
        p(1, i) = r;  % the first row of p is the freq, the second row is smoothed freq r*
        
%         y = (r + 1) * exp(a*log(r + 1) + b) / exp(a * log(r) + b);  % LGT estimates
        y = r * ((1 + 1 / r)^(b + 1)) / num_bi;
        
        % if there is no bigram occurs r+1 times, use y and continue
        if ((i < len) && (index_Nc(1, i + 1) ~= r + 1)) || (i == len) 
            if ~useY
                disp('Warning: reached unobserved count before crossing the smoothing threshold.')
            end
            useY = 1;
        end

        if useY
            p(2, i) = y;  
            continue;
        end
        
        Nrp1 = index_Nc(2, i + 1);
        x = (r + 1) * Nrp1 / Nr / num_bi;  % turing estimates, x = (r+1)*Nr+1/Nr
        
        var = confidencelevel * sqrt(((r + 1) ^ 2) * Nrp1 / (Nr ^ 2) * (1 + Nrp1 / Nr));
        if abs(x - y) > var    % if turing estimates are significantly different from LGT, use turing estimates
            p(2, i) = x;
        else
            useY = 1;
            p(2, i) = y; 
        end
    end
    
    % renormalize
    p_total = sum(p(2, :));
    p(2, :) = (1 - p0_sum) * p(2, :)./p_total;  % normalize the freq of freq
    
    index_0 = [0; p0];
    p = [index_0 p];   % add freq of 0 into the matrix    
    
end