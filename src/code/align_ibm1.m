function AM = align_ibm1(trainDir, maxIter, fn_AM, numSentences)
%
%  align_ibm1
% 
%  This function implements the training of the IBM-1 word alignment algorithm. 
%  We assume that we are implementing P(foreign|english)
%
%  INPUTS:
%
%       dataDir      : (directory name) The top-level directory containing 
%                                       data from which to train or decode
%                                       e.g., '/u/cs401/A2_SMT/data/Toy/'
%       numSentences : (integer) The maximum number of training sentences to
%                                consider. 
%       maxIter      : (integer) The maximum number of iterations of the EM 
%                                algorithm.
%       fn_AM        : (filename) the location to save the alignment model,
%                                 once trained.
%
%  OUTPUT:
%       AM           : (variable) a specialized alignment model structure
%
%
%  The file fn_AM must contain the data structure called 'AM', which is a 
%  structure of structures where AM.(english_word).(foreign_word) is the
%  computed expectation that foreign_word is produced by english_word
%
%       e.g., LM.house.maison = 0.5       % TODO
% 
% Template (c) 2011 Jackie C.K. Cheung and Frank Rudzicz
  
  global CSC401_A2_DEFNS
  
  if (nargin < 2)
     disp( 'lm_prob takes at least 2 parameters');
  elseif nargin == 3
      numSentences = 0;
  end
  
  AM = struct();
  
  % Read in the training data
  [eng, fre] = read_hansard(trainDir, numSentences);

  % Initialize AM uniformly 
  AM = initialize(eng, fre);

%   Iterate between E and M steps
  for iter=1:maxIter
    AM = em_step(AM, eng, fre);
  end

  % Save the alignment model
  save( fn_AM, 'AM', '-mat'); 

  end





% --------------------------------------------------------------------------------
% 
%  Support functions
%
% --------------------------------------------------------------------------------

function [eng, fre] = read_hansard(mydir, numSentences)
%
% Read 'numSentences' parallel sentences from texts in the 'dir' directory.
%
% Important: Be sure to preprocess those texts!
%
% Remember that the i^th line in fubar.e corresponds to the i^th line in fubar.f
% You can decide what form variables 'eng' and 'fre' take, although it may be easiest
% if both 'eng' and 'fre' are cell-arrays of cell-arrays, where the i^th element of 
% 'eng', for example, is a cell-array of words that you can produce with
%
%         eng{i} = strsplit(' ', preprocess(english_sentence, 'e'));
%
  %eng = {};
  %fre = {};

  % TODO: your code goes here.
  
  DD_e = dir( [ mydir, filesep, '*', 'e'] );
  DD_f = dir( [ mydir, filesep, '*', 'f'] );
  if length(DD_e) ~= length(DD_f)
      error('The number of English sentences must be equal to that of the French sentences!');
  end
  
  MAX_LENGTH_SEN = 100000; % specify the size of cell to save time
%   eng = {};
%   fre = {};
  
  
  count = 1;
  if numSentences == 0
      numSentences = realmax;
      eng = cell(1, MAX_LENGTH_SEN);
      fre = cell(1, MAX_LENGTH_SEN);
  else
      eng = cell(1, numSentences);
      fre = cell(1, numSentences);
  end
  for i = 1:length(DD_e)
    if count < numSentences
        lines_e = textread([mydir, filesep, DD_e(i).name], '%s','delimiter','\n');
        lines_f = textread([mydir, filesep, DD_f(i).name], '%s','delimiter','\n');
        for l=1:length(lines_e)
            eng{count} = strsplit(' ', preprocess(lines_e{l}, 'e'));
            fre{count} = strsplit(' ', preprocess(lines_f{l}, 'f'));
            %%%
            for w = 1:length(eng{count})
                if length(eng{count}{w}) > 63
                    eng{count}{w} = eng{count}{w}(1:63);
                end
            end
            for w = 1:length(fre{count})
                if length(fre{count}{w}) > 63
                    fre{count}{w} = fre{count}{w}(1:63);
                end
            end
            %%%
            
            count = count + 1;
            if count > numSentences
                return;
            end
        end
    end
    
  end
end



function AM = initialize(eng, fre)
%
% Initialize alignment model uniformly.
% Only set non-zero probabilities where word pairs appear in corresponding sentences.
%
    AM = struct(); % AM.(english_word).(foreign_word)
    % TODO: your code goes here
    for i=1:length(eng)
        for w_e = 2:(length(eng{i}) - 1)  %exclude sentstart and sentend
            for w_f = 2:(length(fre{i}) - 1)
                AM.(eng{i}{w_e}).(fre{i}{w_f}) = 1;
            end
        end
    end

    eng_words = fieldnames(AM);
    for i = 1:length(eng_words)
        fre_words = fieldnames(AM.(eng_words{i}));
        for f = 1:length(fre_words)
            AM.(eng_words{i}).(fre_words{f}) = 1 / length(fre_words);
        end
    end
    AM.SENTSTART.SENTSTART = 1;
    AM.SENTEND.SENTEND = 1;
end

function t = em_step(t, eng, fre)
% 
% One step in the EM algorithm.  


  % TODO: your code goes here
  t_count = struct();
  t_total = struct();
  for i = 1:length(fre)
%       eng{i}
      e = eng{i}(2:length(eng{i}) - 1);   % exclude SENTSTART and SENTEND
      f = fre{i}(2:length(fre{i}) - 1);
      unique_e = unique(e);
      for w_f = 1:length(f) 
          denom_c = 0;
          for w_eu = 1:length(unique_e)  % unique english word, non-unique french word
              denom_c = denom_c + t.(unique_e{w_eu}).(f{w_f});
              
          end
          if ~isfield(t_count, f{w_f})
              t_count.(f{w_f}) = struct();
          end
          for w_e = 1:length(e)   % non-unique english word, non_unique french word
              if ~isfield(t_count.(f{w_f}), e{w_e})
                  t_count.(f{w_f}).(e{w_e}) = 0;
              end
              t_count.(f{w_f}).(e{w_e}) = t_count.(f{w_f}).(e{w_e}) + t.(e{w_e}).(f{w_f}) / denom_c;
              
              if ~isfield(t_total, e{w_e})
                  t_total.(e{w_e}) = 0;
              end
              t_total.(e{w_e}) = t_total.(e{w_e}) + t.(e{w_e}).(f{w_f}) / denom_c;
          end
      end
  end
  
  for i = 1:length(fre)
      eu = unique(eng{i});
      fu = unique(fre{i});
      for w_eu = 1:length(eu)
          if (strcmp(eu{w_eu}, 'SENTSTART') || strcmp(eu{w_eu}, 'SENTEND'))
              continue;
          end
          for w_fu = 1:length(fu)
              if (strcmp(fu{w_fu}, 'SENTSTART') || strcmp(fu{w_fu}, 'SENTEND'))
                  continue;
              end
              t.(eu{w_eu}).(fu{w_fu}) = t_count.(fu{w_fu}).(eu{w_eu}) / t_total.(eu{w_eu});   
          end
      end
  end
end
    
