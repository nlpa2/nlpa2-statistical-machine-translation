% this function tests for each english word e, whether sumP(f|e) over all
% the french words equals to 1. If it is not 1, display the english word
% and the difference between the sum probability and 1.

function result = test_AM_integrity(AM_name)
    result = 0;
    AM = importdata(AM_name, '-mat');
    eng = fieldnames(AM);
    for e=1:length(eng)
        fre = fieldnames(AM.(eng{e}));
        count = 0;
        for f=1:length(fre)
            count = double (count + AM.(eng{e}).(fre{f}));
        end
        if (abs(count - 1) - e-15) > 0   % if the difference between count and 1 is larger than e-15, display it
            disp(eng{e});
            disp(double(count - 1));
            disp(' ');
            result = count;
        end
    end
end