warning('off', 'MATLAB:REGEXP:deprecated');
load('LM_en', '-mat');
lm_type = 'smooth'; % smooth
delta = 0.001;
vocabSize = length(fieldnames(LM.uni));

testDir = '../data/Hansard/Task5';
test_f = dir([testDir, filesep,'*.f']);
test_f = {test_f.name}';

fre = {};
fre_text = textread([testDir, filesep, test_f{1}], '%s', 'delimiter', '\n');
for i = 1:length(fre_text)
    fre{i} = preprocess(fre_text{i}, 'f');
end

% f = fopen('./output_decode_mle.txt', 'w');
% 
% eng = {};
% fprintf(f, '-----------AMFE_1K_20iter-----------\n');
% load('AM_1K', '-mat');
% disp('Now in AMFE_1K_20iter\n');
% for i = 1:length(fre)
%     eng{i} = decode( fre{i}, LM, AM, lm_type, delta, vocabSize );
%     disp(strjoin(eng{i}));
%     fprintf(f, [strjoin(eng{i}),'\n']);
% end
% 
% eng = {};
% fprintf(f, '-----------AMFE_10K_20iter-----------\n');
% load('AM_10K', '-mat');
% disp('Now in AMFE_10K_20iter\n');
% for i = 1:length(fre)
%     eng{i} = decode( fre{i}, LM, AM, lm_type, delta, vocabSize );
%     disp(strjoin(eng{i}));
%     fprintf(f, [strjoin(eng{i}),'\n']);
% end
% 
% eng = {};
% fprintf(f, '-----------AMFE_15K_20iter-----------\n');
% load('AM_15K', '-mat');
% disp('Now in AMFE_15K_20iter\n');
% for i = 1:length(fre)
%     eng{i} = decode( fre{i}, LM, AM, lm_type, delta, vocabSize );
%     disp(strjoin(eng{i}));
%     fprintf(f, [strjoin(eng{i}),'\n']);
% end
% 
% eng = {};
% fprintf(f, '-----------AMFE_30K_20iter-----------\n');
% load('AM_30K', '-mat');
% disp('Now in AMFE_30K_20iter');
% for i = 1:length(fre)
%     eng{i} = decode( fre{i}, LM, AM, lm_type, delta, vocabSize );
%     disp(strjoin(eng{i}));
%     fprintf(f, [strjoin(eng{i}),'\n']);
% end
% 
% fclose(f);


test_e = dir([testDir, filesep,'*.e']);
test_e = {test_e.name}';

eng_ref = {{}};
for i = 1:length(test_e)
    eng_ref_text = textread([testDir, filesep, test_e{i}], '%s', 'delimiter', '\n');
    for j = 1:length(eng_ref_text)
        eng_ref_temp = preprocess(eng_ref_text{j}, 'e');
        eng_ref{j}{i} = eng_ref_temp(11:end-8); %remove sentstart sentend
    end
end

eng = {};
score = {{{}}};
vocabSize = length(fieldnames(LM.uni));
n = 3;

output_decode = textread('./output_decode2_yuan.txt','%s','delimiter','\n');
% to_remove = [1,27,53,79];
% output_decode(to_remove) = [];
eng{1} = output_decode(1:25); 
eng{2} = output_decode(26:50);
eng{3} = output_decode(51:75);
eng{4} = output_decode(76:100);

scorefile = fopen('./BLEU_score_yuan.txt','w');
amname = {'1K','10K','15K','30K'};
for k = 1:4
    fprintf(scorefile, '=====================AM_%s=====================\n', amname{k});
    fprintf(scorefile, '        unigram         bigram         3-gram\n');
    for i = 1:length(fre)
        eng_words = strsplit(' ',eng{k}{i});
        % disp(['AM-',int2str(k),', No.',int2str(i)]);
        eng_words = eng_words(~strcmp(eng_words,'SENTSTART'));
        eng_words = eng_words(~strcmp(eng_words,'SENTEND'));
        score{i}{k} = BLEU_score(eng_ref{i}, eng_words, n);
        fprintf(scorefile, 'No.%2d   ', i);
        for j = 1:length(score{i}{k})
            fprintf(scorefile, '%f       ',score{i}{k}{j});
        end
        fprintf(scorefile, '\n');
    end
end

fclose(scorefile);

