train_path_e = dir('../data/Hansard/Training/*.e');
train_path_e = {train_path_e.name}';
train_path_f = dir('../data/Hansard/Training/*.f');
train_path_f = {train_path_f.name}';
test_path_e = dir('../data/Hansard/Testing/*.e');
test_path_e = {test_path_e.name}';
test_path_f = dir('../data/Hansard/Testing/*.f');
test_path_f = {test_path_f.name}';

f1 = fopen('../english_train.txt', 'w');
f2 = fopen('../french_train.txt', 'w');
f3 = fopen('../english_test.txt', 'w');
f4 = fopen('../french_test.txt', 'w');

for i = 1:numel(train_path_e)
    train_efile = textread(strcat('../data/Hansard/Training/',train_path_e{i}), '%s', 'delimiter', '\n');
    train_ffile = textread(strcat('../data/Hansard/Training/',train_path_f{i}), '%s', 'delimiter', '\n');
    for j = 1:length(train_efile)
        english_train = preprocess(train_efile{j}, 'e');
        french_train = preprocess(train_ffile{j}, 'f');
        fprintf(f1, '%s\n', english_train);
        fprintf(f2, '%s\n', french_train);
    end
end

for i = 1:numel(test_path_e)
    test_efile = textread(strcat('../data/Hansard/Testing/',test_path_e{i}), '%s', 'delimiter', '\n');
    test_ffile = textread(strcat('../data/Hansard/Testing/',test_path_f{i}), '%s', 'delimiter', '\n');
    for j = 1:length(test_efile)
        english_test = preprocess(test_efile{j}, 'e');
        french_test = preprocess(test_ffile{j}, 'f');
        fprintf(f3, '%s\n', english_test);
        fprintf(f4, '%s\n', french_test);
    end
end


% english_train = {};
% french_train = {};
% english_test = {};
% french_test = {};
% 
% for i = 1:length(train_efile)
%     english_train{i} = preprocess(train_efile{i}, 'e');
%     french_train{i} = preprocess(train_ffile{i}, 'f');
% end
% 
% for i = 1:length(test_efile)
%     english_test{i} = preprocess(test_efile{i}, 'e');
%     french_test{i} = preprocess(test_ffile{i}, 'f');
% end



