load('gt_p', '-mat');
prob_w = struct();

num_uni = sum(cell2mat(struct2cell(LM.uni)));
words1 = fieldnames(LM.uni);
for i = 1:length(words1)

    prob_w.(words1{i}) = 0;
    if isfield(LM.bi, words1{i})
        words2 = fieldnames(LM.bi.(words1{i}));
%     count_denominator = LM.uni.(words1{i});
        for j = 1:length(words2)
            index = LM.bi.(words1{i}).(words2{j});
            prob_w.(words1{i}) = prob_w.(words1{i}) + sum(prod([(p(1, :)==index); p(2, :)], 1)); 
        end
        prob_w.(words1{i}) = prob_w.(words1{i}) + p(2, 1) * (num_uni - length(words2));
    else
        prob_w.(words1{i}) = p(2, 1) * num_uni;
    end
    prob_w.(words1{i}) = prob_w.(words1{i}) / LM.uni.(words1{i}) / num_uni;
end