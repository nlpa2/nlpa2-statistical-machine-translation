function p= gt_freq_of_freq(LM_name)
%
%  gt_freq_of_freq
% 
%  This function computes the smoothed probability of bigrams in LM using 
%  simple-good-turing method described by
%  Gale and Sampson's (1995/2001) "Simple Good Turing" algorithm. 
%  
%  REFERENCE:
%  William Gale and Geoffrey Sampson. 1995. Good-Turing frequency estimation
%  without tears. Journal of Quantitative Linguistics, vol. 2, pp. 217--37.
%
%  INPUT:
%
%       LM_name   : (string)the name of LM structure (the filename)
%  
% 
%  OUTPUT:
%       p         : (matrix) 
%                   p(1, :) is the number of times the bigram occurred in LM
%                   starting from 0 (for unobserved bigrams) to the max
%                   frequency in ascending order
%                   p(2, :) is the smoothed probability correponding to the
%                   number of time in p(1, :)
%            
% 
% 
    load(LM_name, '-mat');

    % convert LM.bi to  a cell of matrix
    bi_temp = struct2cell(structfun(@(x) cell2mat(struct2cell(x)), LM.bi, 'UniformOutput', false));
    
    % convert the cell to matrix and get the max value of frequency in bigram
    bi_mat  =cell2mat(bi_temp);
    max_freq = max(bi_mat);
    num_bi = sum(bi_mat); % total number of bigrams observed
    num_uni = sum(cell2mat(struct2cell(LM.uni)));   % total number of unigrams observed
    num_unseen = num_uni^2 - num_bi;  % number of bigrams unobserved = total number of all possible bigrams in LM - number of bigrams observed in LM
    
    index = (0:max_freq);
    Nc = arrayfun(@(x) sum(bi_mat == x), index);  % the ith element in Nc is Ni-1 the number of bigrams occurred (i-1) times 
    index_Nc = [index; Nc]; % [0, 1, 2, ..., max_freq; N0, N1, N2, ..., Nmax_freq]
    
    p0_sum = index_Nc(2, 2) / num_bi; % sum of p for all unseen bigrams, used for renormalize
    p0 = index_Nc(2, 2) / (num_bi * num_unseen);  % keep a record of p0
    
    index_Nc(:, prod(index_Nc, 1) == 0) = []; % delete the zeros
    len = length(index_Nc);
    
    % average the freq by its adjacent non-zero freqs
    index1 = [index_Nc(1, 1), index_Nc(1, 1:len - 1)];
    index2 = [index_Nc(1, 2:len), 2 * index_Nc(1, len) + index_Nc(1, len - 1)];
    sm_index = index2 - index1;  % [2-1, 3-1, 4-2, 5-3, ..., len-(len-2), 2(len)]
    index_Nc(2, :) = 2 * index_Nc(2, :)./ sm_index;

    p = simple_gt(index_Nc, p0, p0_sum, num_bi); % run good turing smoothing
    
    save(strcat('gt_p_', LM_name), 'p', '-mat');
end


function p = simple_gt(index_Nc, p0, p0_sum, num_bi)    
     % smooth
    len = length(index_Nc);
    r_mat = [log(index_Nc(1, :)); ones(1, len)]';
    Nr_mat = log(index_Nc(2, :))';
    coeff = regress(Nr_mat, r_mat);  % linear regression of Nr_mat and r_mat: r_mat*coeff = Nr_mat
    a = coeff(1, 1);
    b = coeff(2, 1);
    useY = 0;
    p = zeros(2, len);
    
    for i = 1:len
        r = index_Nc(1, i);
        Nr = index_Nc(2, i);
        
        p(1, i) = r;  % the first row of p is the freq, the second row is smoothed freq r*
        
%         y = (r + 1) * exp(a*log(r + 1) + b) / exp(a * log(r) + b);  % LGT estimates
        y = r * ((1 + 1 / r)^(b + 1)) / num_bi;
        
        % if there is no bigram occurs r+1 times, use y and continue
        if ((i < len) && (index_Nc(1, i + 1) ~= r + 1)) || (i == len) 
            if ~useY
                disp('Warning: reached unobserved count before crossing the smoothing threshold.')
            end
            useY = 1;
        end

        if useY
            p(2, i) = y;  
            continue;
        end
        
        Nrp1 = index_Nc(2, i + 1);
        x = (r + 1) * Nrp1 / Nr / num_bi;  % turing estimates, x = (r+1)*Nr+1/Nr
        
        var = 1.65 * sqrt(((r + 1) ^ 2) * Nrp1 / (Nr ^ 2) * (1 + Nrp1 / Nr));
        if abs(x - y) > var    % if turing estimates are significantly different from LGT, use turing estimates
            p(2, i) = x;
        else
            useY = 1;
            p(2, i) = y; 
        end
    end
    
    % renormalize
    p_total = sum(p(2, :));
    p(2, :) = (1 - p0_sum) * (p(2, :)./p_total);  % normalize the freq of freq
    
    index_0 = [0; p0];
    p = [index_0 p];   % add freq of 0 into the matrix    
    
end

