function logProb = lm_prob_good_turing(sentence, LM, p)
%
%  lm_prob_good_turing
% 
%  This function computes the LOG probability of a sentence, given a 
%  language model and apply simple-good-turing smoothing
%
%  INPUTS:
%
%       sentence  : (string) The sentence whose probability we wish
%                            to compute
%       LM        : (variable) the LM structure (not the filename)
%       p         : good-turing smoothed result for the probability of
%       bigrams in LM


  % some rudimentary parameter checking
  if (nargin < 2)
    disp( 'lm_prob takes at least 2 parameters');
    return;
  end
  
  words = strsplit(' ', sentence);

  num_uni = sum(cell2mat(struct2cell(LM.uni)));
  logProb = 0;
  
  for i = 1:length(words) - 1
    
     % for any word not occurring in LM, set logProb to -Inf 
     if ~isfield(LM.uni, words{i}) || ~isfield(LM.uni, words{i + 1})
         logProb = -Inf;
         continue;
     else
         count_denominator = LM.uni.(words{i}) / num_uni;   % probability of unigram p(wt)
         if ~isfield(LM.bi, words{i}) || ~isfield(LM.bi.(words{i}), words{i + 1})
             count_numerator = p(2, 1);
         else
             index = LM.bi.(words{i}).(words{i+1});  % the number of times 'words{i} words{i + 1}' occurs in LM
             count_numerator = sum(prod([(p(1, :)==index); p(2, :)], 1));      % for the number of times 'index', get the correponding smoothed probability in p
         end
     end

     logProb = logProb + log2(count_numerator) - log2(count_denominator);

  end
return
end