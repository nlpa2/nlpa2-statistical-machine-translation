function pp = perplexity_bonus( LM_name, testDir, language)
%
%  perplexity
% 
%  This function computes the perplexity of language model given a test corpus using good_turing smoothing
%
%  INPUTS:
%
%       LM_name        : (string) name of the language model previously trained by lm_train
%       testDir   : (directory name) The top-level directory containing 
%                   data from which to compute perplexity
%                   e.g., '/u/cs401/A2_SMT/data/Hansard/Testing/'
%       language  : (string) either 'e' for English or 'f' for French 
%
% 
% Template (c) 2011 Frank Rudzicz

global CSC401_A2_DEFNS

DD        = dir( [ testDir, filesep, '*', language] );
pp        = 0;
N         = 0;

if (nargin ~= 3)
    disp( 'The function takes exactly 3 arguments.');
    return;
end

% load the langauge model and corresponding p matrix
load(LM_name, '-mat');
load(strcat('gt_p_', LM_name), '-mat');


for iFile=1:length(DD)

  lines = textread([testDir, filesep, DD(iFile).name], '%s','delimiter','\n','bufsize', 8190);

  for l=1:length(lines)

    processedLine = preprocess(lines{l}, language);
    tpp = lm_prob_good_turing(processedLine, LM, p);  % changed to good turing
    if (tpp > -Inf)   % only consider sentences that have some probability 
      pp = pp + tpp;
      words = strsplit(' ', processedLine);
      N = N + length(words);
    end
  end
end

pp = 2^(-pp/N);
return