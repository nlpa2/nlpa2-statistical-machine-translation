function outSentence = preprocess( inSentence, language )
%
%  preprocess
%
%  This function preprocesses the input text according to language-specific rules.
%  Specifically, we separate contractions according to the source language, convert
%  all tokens to lower-case, and separate end-of-sentence punctuation 
%
%  INPUTS:
%       inSentence     : (string) the original sentence to be processed 
%                                 (e.g., a line from the Hansard)
%       language       : (string) either 'e' (English) or 'f' (French) 
%                                 according to the language of inSentence
%
%  OUTPUT:
%       outSentence    : (string) the modified sentence
%
%  Template (c) 2011 Frank Rudzicz 

  global CSC401_A2_DEFNS
  
  % first, convert the input sentence to lower-case and add sentence marks 
  inSentence = [CSC401_A2_DEFNS.SENTSTART ' ' lower( inSentence ) ' ' CSC401_A2_DEFNS.SENTEND];

  % trim whitespaces down 
  inSentence = regexprep( inSentence, '\s+', ' '); 

  % initialize outSentence
  outSentence = inSentence;

  % perform language-agnostic changes
  % TODO: your code here
  %    e.g., outSentence = regexprep( outSentence, 'TODO', 'TODO');
  
  % Step 1: Separate sentence-final punctuation, including .?!
  outSentence = regexprep( outSentence, '([\.\?\!])', ' $1 ', 'tokenize');
  
  % Step 2: Separate ,:;()(-)+<>=", 8-9  VS (8-9)
  outSentence = regexprep( outSentence, '([,:;\(\)\+<>="`])', ' $1 ', 'tokenize'); 
  outSentence = regexprep( outSentence, '(\d\s*)(-)(\s*\d)', '$1 $2 $3','tokenize');
  
  % Step 3: Separate dashes between parentheses
  % outSentence = regexprep( outSentence, '(\([^\(\)]*)(-)([^\(\)]*\))', '$1 $2 $3', 'tokenize');
  matchStr = regexpi( outSentence, '(\(.+\))', 'match');
  matchStr = regexprep(matchStr, '(-)', ' $1 ', 'tokenize');
  outSentence = regexprep( outSentence, '(\(.+\))', matchStr, 'tokenize'); 

  switch language
   case 'e'
    % TODO: your code here
    % Separate possessives & clitics
    % dog''s -> dog ''s, (s|m|re|d|ll|ve)
    outSentence = regexprep( outSentence, '(\w+)('')(s|m|re|d|ll|ve)', '$1 $2$3', 'tokenize'); 
    % dogs'' -> dogs ''
    outSentence = regexprep( outSentence, '(s)('')', '$1 $2', 'tokenize'); 
    % can''t -> ca n''t
    outSentence = regexprep( outSentence, '(\w+)(n''t)', '$1 $2', 'tokenize'); 
    % 'Star Trek' -> ' Star Trek '
    outSentence = regexprep( outSentence, '([^s])('')(?!(s |m |re |d |ll |ve |t ))', '$1 $2 ', 'tokenize');
    
   case 'f'
    % TODO: your code here
    % Separate possessives & clitics
    % I'election => I' election
    outSentence = regexprep( outSentence, '(I'')(\w+)', '$1 $2', 'tokenize'); 
    % ce, de, je, le, me, ne, que(not single-consonant), se, te ->
    % [cdjlmnst], je t'aime => je t' aime. 
    outSentence = regexprep( outSentence, '([cjlmnst]'')(\w+)', '$1 $2', 'tokenize');
    outSentence = regexprep( outSentence, '(d'')(?!(abord|accord|ailleurs|habitude))', '$1 ', 'tokenize');
    % qu'on => qu' on
    outSentence = regexprep( outSentence, '(qu'')(\w+)', '$1 $2', 'tokenize'); 
    % puisque'on => puisque' on, lorsqu'il => lorsqu' il
    outSentence = regexprep( outSentence, '((puisque|lorsqu)'')(on|il)', '$1 $2', 'tokenize');

  end

  % trim whitespaces down 
  outSentence = regexprep( outSentence, '\s+', ' '); 
  
  % change unpleasant characters to codes that can be keys in dictionaries
  outSentence = convertSymbols( outSentence );

