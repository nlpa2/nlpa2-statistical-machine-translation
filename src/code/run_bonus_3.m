% this script runs the bonus part: apply Good-Turing Smoothing to LM and
% get perplexity
csc401_a2_defns;
LM_names = {'LM_en', 'e'; 'LM_fn', 'f'};
testDir = '../data/Hansard/Testing';
pp = struct();
for i=1:length(LM_names)
    % the gt_p files have been saved as gt_p_LM_en and gt_p_LM_fn
    % and can be loaded directly
    % run the following line if they do not exist
    %gt_freq_of_freq(LM_names{i, 1});
    
    pp.(LM_names{i, 2}) = perplexity_bonus( LM_names{i, 1}, testDir, LM_names{i, 2});
    disp(strcat('pp_', LM_names{i, 2}))
    disp(pp.(LM_names{i, 2}));
end
    