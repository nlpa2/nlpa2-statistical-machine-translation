%
% evalAlign
%
%  This is simply the script (not the function) that you use to perform your evaluations in 
%  Task 5. 

% some of your definitions
trainDir     = '/u/cs401/A2_SMT/data/Hansard/Training';
testDir      = '/u/cs401/A2_SMT/data/Hansard/Testing';
fn_LME       = './LM_en';
fn_LMF       = './LM_fn';
fn_AM       = './AM'; 
numSentences = {1000, 10000, 15000, 30000};
warning('off', 'MATLAB:REGEXP:deprecated');
% Train your language models. This is task 2 which makes use of task 1
LME = lm_train( trainDir, 'e', fn_LME );
LMF = lm_train( trainDir, 'f', fn_LMF );

% load('LM_en', '-mat');
% LME = LM;

% Train your alignment model of French, given English 
AMFE = {};
maxIter = 20;
for i = 1:length(numSentences) 
    AMFE{i} = align_ibm1( trainDir, maxIter, [fn_AM,'_',int2str(numSentences{i})], numSentences{i});
end
% ... TODO: more 

% AMFE = {};
% load('AM_1K', '-mat');
% AMFE{1} = AM;
% load('AM_10K', '-mat');
% AMFE{2} = AM;
% load('AM_15K', '-mat');
% AMFE{3} = AM;
% load('AM_30K', '-mat');
% AMFE{4} = AM;
% TODO: a bit more work to grab the English and French sentences. 
%       You can probably reuse your previous code for this  

% Read french sentences
test_f = dir([testDir, filesep,'Task5*.f']);
test_f = {test_f.name}';

fre = {};
fre_text = textread([testDir, filesep, test_f{1}], '%s', 'delimiter', '\n');
for i = 1:length(fre_text)
    fre{i} = preprocess(fre_text{i}, 'f');
end

% Read reference english translations
% add BlueMix code here 
username = '6433d9ca-aca2-478c-b7a2-4fa8d76ebeaf';
password = 'nFZf3nzHxKUX';

f = fopen([testDir, filesep, 'Task5.ibm.e'], 'w');
for i = 1:length(fre_text)
    text = fre_text{i};
    [status, result] = unix(['curl -u ',username,':',password,' -X POST -F "text=',text,'" -F "source=fr" -F "target=en" "https://gateway.watsonplatform.net/language-translation/api/v2/translate"']);
    fprintf(f,[result,'\n']);
end
fclose(f);

test_e = dir([testDir, filesep,'Task5*.e']);
test_e = {test_e.name}';

eng_ref = {{}};
for i = 1:length(test_e)
    eng_ref_text = textread([testDir, filesep, test_e{i}], '%s', 'delimiter', '\n');
    for j = 1:length(eng_ref_text)
         eng_ref_temp = preprocess(eng_ref_text{j}, 'e');
         eng_ref{j}{i} = eng_ref_temp(11:end-8); %remove sentstart sentend
    end
end

% Decode the test sentence 'fre'
eng = {{}};
score = {{{}}};
vocabSize = length(fieldnames(LME.uni));
n = 3;
scorefile = fopen('./BLEU_score_decode2_gt.txt','w');
amname = {'1K','10K','15K','30K'};
load('gt_p_LM_en','-mat');
decode2_output = fopen('./decode2_output_gt.txt','w');
for k = 1:length(AMFE)
    fprintf(scorefile, '=====================AM_%s=====================\n', amname{k});
    fprintf(scorefile, '        unigram         bigram         3-gram\n');
    fprintf(decode2_output, '=====================AM_%s=====================\n', amname{k});
    for i = 1:length(fre)
%         eng{k}{i} = decode( fre{i}, LME, AMFE{k}, lm_type, delta, vocabSize );
%         score{i}{k} = BLUE_score(eng_ref{i}, eng{k}{i}, n);
        eng{k}{i} = decode2_bonus( fre{i}, LME, AMFE{k}, p);
        fprintf(decode2_output,[eng{k}{i},'\n']);
        eng_words = strsplit(' ',eng{k}{i});
        eng_words = eng_words(~strcmp(eng_words,'SENTSTART'));
        eng_words = eng_words(~strcmp(eng_words,'SENTEND'));
        score{i}{k} = BLEU_score(eng_ref{i}, eng_words, n);
        fprintf(scorefile, 'No.%2d   ', i);
        for j = 1:length(score{i}{k})
            fprintf(scorefile, '%f       ',score{i}{k}{j});
        end
        fprintf(scorefile, '\n');
    end
end

fclose(scorefile);



