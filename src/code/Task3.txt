-----------English-----------
No smooth:      pp = 13.569899
delta = 0.0001: pp = 42.970718
delta = 0.005:  pp = 40.122472
delta = 0.001:  pp = 39.406664
delta = 0.05:   pp = 51.930279
delta = 0.01:   pp = 41.887015
delta = 0.1:    pp = 60.391317
delta = 0.2:    pp = 73.033924
delta = 0.5:    pp = 100.009857
delta = 1:      pp = 133.188826
-----------French-----------
No smooth:      pp = 13.013710
delta = 0.0001: pp = 38.384424
delta = 0.005:  pp = 38.402943
delta = 0.001:  pp = 36.344736
delta = 0.05:   pp = 53.500972
delta = 0.01:   pp = 40.907079
delta = 0.1:    pp = 63.782747
delta = 0.2:    pp = 79.168349
delta = 0.5:    pp = 112.561395
delta = 1:      pp = 154.695615

As we can see from the data above, the perplexity of MLE is much lower than that one of smoothing. There are two possible reasons: 
1. When computing perplexity, it only considers sentences that have finite probability, whereas discard the case with probability of 0/0. Thus, MLE will skip all unobserved cases, which results in lower perplexity.
2. The probability of the observed grams will be reduced by the smoothing step because we amortize the probabilities over those unseen n-grams. That may result in a smaller sum of log probabilities and higher perplexity. 

When we try different values of delta, the result seems to reach a minima at delta = 0.001 (both in english and french). The value of delta represents how much probability mass we will give to unseen n-grams. When delta = 1, we are quite generous and may give too much, which can significantly increase the perplexity; while delta = 0.0001, we just give a little probability to them, making the model closer to MLE (no smoothing).