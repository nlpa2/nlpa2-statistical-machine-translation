 function score = BLEU_score(eng_ref, can_words, n)

% This function computes BLEU_score for the decoded sentence.
% @eng_ref: 3 references from Hansards, google and ibm
% @eng: decoded english
% @n: n-value of BLEU
% @rtype: BLEU score 

    ref_words = {};
    diff = realmax;
    candidate_len = length(can_words); 
    similar_len = 0;
    for i = 1:length(eng_ref)
        ref_words{i} = strsplit(' ', eng_ref{i});
        reference_len{i} = length(ref_words{i});
        diff_new = abs(candidate_len - reference_len{i});
        if diff_new < diff
            diff = diff_new;
            similar_len = reference_len{i};
        end
    end
    
    BP = 1;
    if similar_len > candidate_len
        BP = exp(1 - double(similar_len)/(candidate_len)); % 2 for sentstart end sentend
    else
        BP = 1;
    end
    
    p = compute_ngram(ref_words, can_words, n);

    score = [];
    p_chain = 1;
    for idx = 1:n
        p_chain = p_chain * p{idx};
        score{idx} = BP * power(p_chain, 1/idx);
    end
 end

function p = compute_ngram(ref_words, can_words, n)
% build ngram and count ngram precision

% @para ref_words: 3 references wordslist from Hansards, google and ibm    
% @para can_words: candidate translation words dict
% @para n: n-value of BLEU
% @rtype p: precision of ngram
    can_keys = [];
    for l = 1:n
        ngram = [];
        for j = 1:length(can_words)-l+1   
            ngram_str = '';
            for k = j:j+l-1
                ngram_str = [ngram_str ' ' can_words{k}]; 
            end
            ngram{j} = ngram_str(2:end); % remove space in the beginning
        end
        can_keys{l} = ngram;
    end

    ref_keys = {};
    for l = 1:n      
        for i = 1:length(ref_words)
            ngram = [];
            for j = 1:length(ref_words{i})-l+1
                ngram_str = '';
                for  k = j:j+l-1
                    ngram_str = [ngram_str ' ' ref_words{i}{k}];
                end
                ngram{j} = ngram_str(2:end);
            end
            ref_keys{l}{i} = ngram;
        end
    end
            
    ref_keys_cat = {}; 
    for i = 1:length(ref_keys)
        ref_keys_cat{i} = [];
        for j = 1:length(ref_keys{1})
            ref_keys_cat{i} = [ref_keys_cat{i}, ref_keys{i}{j}];
        end
    end
 
    p = [];
    for m = 1:n
%         disp(cellfun(@(s) ~isempty(strmatch(s,ref_keys_cat{m},'exact')), can_keys{m}));
        p{m} = sum(cellfun(@(s) ~isempty(strmatch(s,ref_keys_cat{m},'exact')), can_keys{m}))/length(can_keys{m});       
    end
end



    
